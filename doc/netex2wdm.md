Import Netex vers WDM
=============================

Le document suivant décrit comment les données Netex sont importées et converties au format [WDM](https://gitlab.com/yukaimaps/yukaidi-tagging-schema/-/blob/main/doc/Walk_data_model.md) dans le cadre du projet Yukaimaps.

Les données d'entrées sont attendues conformes au profil France documenté ici :
* https://normes.transport.data.gouv.fr/normes/netex/arrets/
* et plus généralement ici https://normes.transport.data.gouv.fr/

**Table des matières :**

[[_TOC_]]

[node]: https://wiki.openstreetmap.org/w/images/thumb/4/48/Osm_element_node.svg/20px-Osm_element_node.svg.png
[way]: https://wiki.openstreetmap.org/w/images/thumb/4/48/Osm_element_way.svg/20px-Osm_element_way.svg.png
[area]: https://wiki.openstreetmap.org/w/images/thumb/4/48/Osm_element_area.svg/20px-Osm_element_area.svg.png
[relation]:https://wiki.openstreetmap.org/w/images/thumb/4/48/Osm_element_relation.svg/20px-Osm_element_relation.svg.png

# Règles générales

## Identifiants d'import

Trois attributs sont systématiquement ajoutés pour chaque objet WDM créé qui présente des tags :

* Ref:Import:Source : valeur fixe "NeTEx"
* Ref:Import:Id : identifiant de l'objet Netex
* Ref:Import:Version : version de l'objet Netex. Si c'est "any", le tag Ref:Import:Version n'est pas créé

**Remarque**: si un objet WDM est déjà existant avec les tags Ref:Import:Source et Ref:Import:Id exactement identiques, l'objet est ignoré et n'est pas importé.

## Attributs fréquents

### Centroid/Location

L'élément Centroid/Location est utilisé pour représenter une position ponctuelle. L'objet sera alors généralement transformé en ![node] noeud.

Si Centroid/Location/`gml:pos` est présent, l'élément est utilisé pour construire la position du ![node] noeud.
Une projection peut être nécessaire ; le système de coordonnées peut être précisé via l'attribut Centroid/Location/`gml:pos`/`@srsName` ou via l'élément FrameDefaults/DefaultLocationSystem.

Sinon, Centroid/Location/Latitude et Centroid/Location/Longitude sont utilisés pour construire la position du ![node] noeud.

Si Centroid/Location/Altitude est présent, le tag WDM Altitude est renseigné sur le ![node] noeud à partir de l'élément.
 
### Polygon

L'élément Polygon est utilisé pour représenter un polygone. L'objet sera alors généralement transformé en ![area] zone.

Si Polygon/`gml:exterior` est présent, les positions présentes sont utilisées pour construire la géométrie de la ![area] zone.
Une projection peut être nécessaire ; le système de coordonnées peut être précisé via l'attribut Polygon/`gml:pos`/`@srsName` ou via l'élément FrameDefaults/DefaultLocationSystem.

Sinon, les positions présentes dans Polygon/exterior/LinearRing/pos sont utilisées pour construire la géométrie de la ![area] zone.

### AccessibilityAssessment

L'élément AccessibilityAssessment/validityConditions/ValidityCondition[0]/Description est utilisé pour remplir le tag WheelchairAccess:Description.

L'élément AccessibilityAssessment/limitations/AccessibilityLimitation[0]/WheelchairAccess est utilisé pour remplir le tag WheelchairAccess :
* WheelchairAccess=Yes si true
* WheelchairAccess=No si false
* WheelchairAccess=Limited si partial
* tag absent si unknown

L'élément AccessibilityAssessment/limitations/AccessibilityLimitation[0]/AudibleSignalsAvailable est utilisé pour remplir le tag AudibleSignals :
* Yes si true
* No si false
* Limited si partial
* tag absent si unknown

L'élément AccessibilityAssessment/limitations/AccessibilityLimitation[0]/VisualSignsAvailable est utilisé pour remplir le tag VisualSigns :
* Yes si true
* No si false
* Limited si partial
* tag absent si unknown

# Lecture des objets Quay

Les objets Netex Quay sont susceptibles d'être transformés en objets WDM de type Quay=* :

* si l'élément Quay/Polygon est présent, une ![area] zone est créée, avec les tags InsideSpace=Quay et Quay=*
* si l'élément Quay/Centroid est présent, un ![node] noeud est créé, avec le tag Quay=*
* s'il n'y a pas de géométrie, aucun objet n'est créé

Quay/TransportMode est utilisé pour remplir la valeur du tag Quay avec les règles de gestion suivantes :

| Quay/TransportMode | tag Quay   |
| ------------------ | ---------- |
| bus                | Bus        |
| coach              | Coach      |
| ferry              | Water      |
| metro              | Metro      |
| rail               | Rail       |
| trolleyBus         | TrolleyBus |
| tram               | Tram       |
| water              | Water      |
| cableway           | CableWay   |
| funicular          | Funicular  |
| autre valeur       | Other      |

## Conversion des attributs

Le tag WDM Name est rempli à partir de Quay/Name

Le tag WDM Description est rempli à partir de Quay/Description

Les tags WDM suivants peuvent être créés en suivant les règles de conversion communes :
* WheelchairAccess:Description
* WheelchairAccess
* AudibleSignals
* VisualSigns

Le tag WDM PublicCode est rempli à partir de Quay/PublicCode.

Le tag Outdoor est rempli à partir de Quay/Covered :
* Yes si outdoors
* No si indoors
* Covered si covered
* non renseigné sinon

Le tag Gated est rempli à partir de Quay/Gated :
* Yes si gatedArea
* non renseigné sinon

Le tag Lighting est rempli à partir de Quay/Lighting :
* Yes si wellLit
* No si unlit
* Bad si poorlyLit
* non renseigné sinon

# Lecture des objets StopPlace

Les objets Netex StopPlace sont susceptibles d'être transformés en objets WDM de type StopPlace=*.

Si l'objet StopPlace ne contient ni quais ni entrées, il est ignoré et aucun objet WDM n'est créé.

L'inclusion d'un objet Netex Quay dans un objet StopPlace peut prendre la forme suivante :
* l'objet StopPlace comprend un élément StopPlace/quays/Quay avec l'objet Quay
* l'objet StopPlace comprend un élément StopPlace/quays/QuayRef avec une référence vers l'objet Quay
* l'objet Quay comprend un élément ParentZoneRef avec une référence vers l'objet StopPlace
* l'objet Quay comprend un élément SiteRef avec une référence vers l'objet StopPlace

L'inclusion d'un objet Netex Entrance (ou StopPlaceEntrance) dans un objet StopPlace peut prendre la forme suivante :
* l'objet StopPlace comprend un élément StopPlace/entrances/Entrance avec l'objet Entrance
* l'objet StopPlace comprend un élément StopPlace/entrances/StopPlaceEntrance avec l'objet Entrance
* l'objet StopPlace comprend un élément StopPlace/entrances/EntranceRef avec une référence vers l'objet Entrance
* l'objet StopPlace comprend un élément StopPlace/entrances/StopPlaceEntranceRef avec une référence vers l'objet Entrance
* l'objet Entrance ou StopPlaceEntrance comprend un élément SiteRef avec une référence vers l'objet StopPlace

L'objet WDM est créé avec les règles de gestion suivantes :
* si StopPlace/StopPlaceType est onstreetBus / onstreetTram / ferryStop / other, alors l'objet WDM créé une ![relation] relation StopPlace=*
* si StopPlace/StopPlaceType prend une autre valeur, l'objet WDM créé est ![node]/![area] PointOfInterest=StopPlace et StopPlace=*

## Conversion des attributs

Le tag WDM Name est rempli à partir de StopPlace/Name

Le tag WDM Description est rempli à partir de StopPlace/Description

Les tags WDM suivants peuvent être créés en suivant les règles de conversion communes :
* WheelchairAccess:Description
* WheelchairAccess
* AudibleSignals
* VisualSigns

Le tag WDM StopPlace est rempli à partir de StopPlace/StopPlaceType, en passant la première lettre en majuscule.

# Lecture des objets Entrance

Les objets Netex Entrance et StopPlaceEntrance sont susceptibles d'être transformés en objets WDM de type Entrance=StopPlace.

## Conversion des attributs

Le tag WDM Name est rempli à partir de Entrance/Label ou à défaut Entrance/Name.

Le tag WDM AddressLine est rempli à partir de Entrance/PostalAddress/AddressLine1.

Le tag WDM Description est rempli à partir de Entrance/Description.

Les tags WDM suivants peuvent être créés en suivant les règles de conversion communes :
* WheelchairAccess:Description
* WheelchairAccess

Le tag WDM PublicCode est rempli à partir de Entrance/PublicCode.

Les tags WDM EntrancePassage, Door et AutomaticDoor peuvent être remplis à partir de Entrance/EntranceType :

| Entrance/EntranceType | tags WDM                                  |
| --------------------- | ----------------------------------------- |
| opening               | EntrancePassage=Opening                   |
| gate                  | EntrancePassage=Gate                      |
| automaticDoor         | EntrancePassage=Door et AutomaticDoor=Yes |
| revolvingDoor         | EntrancePassage=Door et Door=Revolving    |
| swingDoor             | EntrancePassage=Door et Door=Swing        |
| door                  | EntrancePassage=Door                      |
| autres conditions     | tags non créés                            |

Le tag WDM IsEntry est rempli à partir de Entrance/IsEntry :
* Yes si true
* No si false
* tag absent sinon

Le tag WDM IsExit est rempli à partir de Entrance/IsExit :
* Yes si true
* No si false
* tag absent sinon

Le tag WDM Width est rempli à partir de Entrance/Width.

Le tag WDM Height est rempli à partir de Entrance/Height.

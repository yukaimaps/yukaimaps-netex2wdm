# Netex2WDM

This is the tool that imports NeTEx data to WDM.

## Usage

### Install

Dependencies:

- Python >= 3.7

Launch following commands for running the converter:

```bash
# Init Python environment
python -m venv ./env
source ./env/bin/activate
pip install -e .
```

### Running


### Testing

```bash
pip install -e .[dev]
pytest
```


## Useful links

* [WDM](https://gitlab.com/yukaimaps/yukaidi-tagging-schema/-/blob/main/doc/Walk_data_model.md): documentation of the Walk Data Model (WDM) used here
* [NeTEx XML schema](https://github.com/NeTEx-CEN/NeTEx)

#
# Converts Netex dataset into WDM
#

from lxml import etree
from lxml.builder import E
import pyproj
import traceback
import logging

NAMESPACES = {"netex": "http://www.netex.org.uk/netex", "gml": "http://www.opengis.net/gml/3.2"}
XPATH_FRAME = "netex:dataObjects/*[substring(name(), string-length(name()) - string-length('Frame') + 1) = 'Frame']"
XPATH_FRAME_MEMBERS = f"{XPATH_FRAME}/netex:members"


def readXMLFile(inputPath: str):
	"""Parses XML file

	Parameters
	----------
	inputPath : str
		The file path of the XML to read

	Returns
	-------
	lxml.etree.ElementTree
		The read XML tree
	"""

	with open(inputPath, 'r') as inputFile:
		xml = etree.parse(inputFile)
		return xml


def writeXMLFile(xml: etree.Element, outputPath: str):
	"""Saves XML Element into a text file

	Parameters
	----------
	xml: lxml.etree.Element
		The XML root Element to save
	outputPath: str
		Path to file to write on filesystem
	"""

	et = etree.ElementTree(xml)
	et.write(outputPath, pretty_print=True)


def transformNetexIntoWDM(netex: etree.Element):
	"""Parses a whole Netex XML Element and returns WDM XML equivalent

	Parameters
	----------
	netex: lxml.etree.Element
		The Netex XML root element

	Returns
	-------
	lxml.etree.Element
		The WDM equivalent
	"""

	# Create result WDM
	wdm = E.osm(version="0.6", generator="Yukaimaps Netex2WDM")

	# Read FrameDefaults
	frameDefaults = netex.xpath(f"{XPATH_FRAME}/netex:FrameDefaults", namespaces=NAMESPACES)
	frameDefaults = frameDefaults[0] if len(frameDefaults) == 1 else None


	# Parse quays
	netexQuays = list(netex.xpath(f"{XPATH_FRAME_MEMBERS}/netex:Quay", namespaces=NAMESPACES)) # Quays directly as members
	netexQuays += list(netex.xpath(f"{XPATH_FRAME_MEMBERS}/netex:StopPlace/netex:quays/netex:Quay", namespaces=NAMESPACES)) # Quays from StopPlaces

	for netexQuay in netexQuays:
		try:
			appendQuay(wdm, netexQuay, frameDefaults)
		except Exception:
			logging.exception("Skipped quay", exc_info=True)

	# Parse entrances
	netexEntrances = list(netex.xpath(f"{XPATH_FRAME_MEMBERS}/netex:Entrance", namespaces=NAMESPACES)) # Entrances directly as members
	netexEntrances = list(netex.xpath(f"{XPATH_FRAME_MEMBERS}/netex:StopPlaceEntrance", namespaces=NAMESPACES)) # StopPlaceEntrances directly as members
	netexEntrances += list(netex.xpath(f"{XPATH_FRAME_MEMBERS}/netex:StopPlace/netex:entrances/netex:Entrance", namespaces=NAMESPACES)) # Entrances from StopPlaces
	netexEntrances += list(netex.xpath(f"{XPATH_FRAME_MEMBERS}/netex:StopPlace/netex:entrances/netex:StopPlaceEntrance", namespaces=NAMESPACES)) # Entrances from StopPlaces

	for netexEntrance in netexEntrances:
		try:
			appendEntrance(wdm, netexEntrance, frameDefaults)
		except Exception:
			logging.exception("Skipped entrance", exc_info=True)

	# Parse stop places
	netexStops = list(netex.xpath(f"{XPATH_FRAME_MEMBERS}/netex:StopPlace", namespaces=NAMESPACES))

	for netexStop in netexStops:
		try:
			appendStopPlace(wdm, netexStop, getStopPlaceQuays(netex, netexStop.get("id")), getStopPlaceEntrances(netex, netexStop.get("id")), frameDefaults)
		except Exception:
			logging.exception("Skipped stop place", exc_info=True)


	return wdm


def gmlPosToLonLat(pos: etree.Element, srs: str = None):
	"""Transforms a single GML pos Element into lon/lat coordinates

	Parameters
	----------
	pos : lxml.etree.Element
		The GML pos
	srs : str
		The SRS identifier

	Returns
	-------
	list
		Coordinates as [lon, lat, altitude] in EPSG:4326
		Altitude may be None if not set in input data
	"""

	coords = pos.text.strip().split(" ")
	lon, lat = coords[0:2]
	alt = coords[2] if len(coords) == 3 else None
	srs = pos.get("srsName") or srs

	if srs and srs != "EPSG:4326":
		transformer = pyproj.Transformer.from_crs(srs, "EPSG:4326")
		lat, lon = transformer.transform(lon, lat)
	else:
		lon = float(lon)
		lat = float(lat)

	return [f"{lon:.7f}", f"{lat:.7f}", alt]


def netexCentroidToLonLat(centroid: etree.Element, srs: str = None):
	"""Parses lon/lat coordinates from Netex Centroid Element

	Parameters
	----------
	centroid : lxml.etree.Element
		The Netex Centroid
	srs : str
		The default SRS to use if no one is set on position

	Returns
	-------
	list
		Coordinates as [lon, lat, altitude] in EPSG:4326
		Altitude may be None if not set in input data
	"""

	# Reads lon/lat/alt from markup variants
	pos = centroid.xpath("netex:Location/gml:pos", namespaces=NAMESPACES)
	longt = centroid.xpath("netex:Location/netex:Longitude", namespaces=NAMESPACES)
	lattd = centroid.xpath("netex:Location/netex:Latitude", namespaces=NAMESPACES)
	altit = centroid.xpath("netex:Location/netex:Altitude", namespaces=NAMESPACES)

	if pos:
		pos = pos[0]
		return gmlPosToLonLat(pos, srs)

	elif longt and lattd:
		lon = longt[0].text
		lat = lattd[0].text
		alt = altit[0].text if len(altit) == 1 else None

		# Reproject coordinates
		if srs and srs != "EPSG:4326":
			transformer = pyproj.Transformer.from_crs(srs, "EPSG:4326")
			lat, lon = transformer.transform(lon, lat)
		else:
			lon = float(lon)
			lat = float(lat)

		return [f"{lon:.7f}", f"{lat:.7f}", alt]

	else:
		raise Exception("Centroid format is not supported")


def appendNetexPolygonToWdm(netex: etree.Element, wdm: etree.Element, srs: str = None):
	"""Reads Polygon data from Netex Element, and inserts it into WDM dataset

	Parameters
	----------
	netex : lxml.etree.Element
		The Netex object to read Polygon from
	wdm : lxml.etree.Element
		The WDM container ("osm" Element) to insert data into
	srs : str
		The default SRS to use if no one is set on position

	Returns
	-------
	lxml.etree.Element
		The way representing the inserted polygon
	"""

	polygon = netex.xpath("gml:Polygon", namespaces=NAMESPACES)

	if len(polygon) != 1:
		raise Exception("No Polygon found")

	polygon = polygon[0]
	if polygon.get("srsName"):
		srs = polygon.get("srsName")

	# Find linear ring (= list of positions)
	linearRing = polygon.xpath("gml:exterior/gml:LinearRing", namespaces=NAMESPACES)

	if len(linearRing) != 1:
		raise Exception("No LinearRing found in Polygon")

	linearRing = linearRing[0]

	# Extract coordinates
	positions = linearRing.xpath("gml:pos", namespaces=NAMESPACES)

	if len(positions) < 4:
		raise Exception("LinearRing may contain at least 4 positions (with first and last being the same)")
	elif positions[0].text != positions[-1].text:
		raise Exception("Last position is different from first one")

	# Append coordinates as nodes in WDM
	wdmNodeId = int(getLastNewWDMElementId(wdm, "node") or "0") - 1
	wdmPolyNodeRefs = []
	del positions[-1] # Remove last element

	for i in range(0, len(positions)):
		pos = positions[i]
		lonlat = gmlPosToLonLat(pos, srs)
		node = E.node(id=str(wdmNodeId), lon=lonlat[0], lat=lonlat[1])
		wdmPolyNodeRefs.append(str(wdmNodeId))

		if len(lonlat) == 3 and lonlat[2] is not None:
			node.append(E.tag(k="Altitude", v=lonlat[2]))

		wdm.append(node)
		wdmNodeId -= 1

	wdmPolyNodeRefs.append(wdmPolyNodeRefs[0]) # Add first node as last one

	# Append polygon as way in WDM
	wdmWayId = int(getLastNewWDMElementId(wdm, "way") or "0") - 1
	way = E.way(id=str(wdmWayId))
	for nr in wdmPolyNodeRefs:
		way.append(E.nd(ref=nr))

	wdm.append(way)

	return way


def getLastNewWDMElementId(wdm: etree.Element, elementType: str):
	"""Checks out in a WDM dataset what is the last ID used for a created feature (negative IDs)

	Parameters
	----------
	wdm : lxml.etree.Element
		The WDM container ("osm" Element) to analyze
	elementType : str
		The type of element to check (node, way, relation)

	Returns
	-------
	str
		The last used created feature ID
	"""

	lastId = None

	wdmElements = wdm.xpath(elementType)

	for e in wdmElements:
		eid = e.get("id")
		if eid is not None:
			eid = int(eid)
			if eid < 0:
				if lastId is None:
					lastId = eid
				elif lastId > eid:
					lastId = eid

	return str(lastId) if lastId is not None else None


def getChildText(elem: etree.Element, childTag: str):
	"""Get the text of first child of an element (if any)

	Parameters
	----------
	elem: lxml.etree.Element
		The element to search into
	childTag: str
		The child tag. Also support XPath expressions

	Returns
	-------
	str or None
		Text of first found child if any
	"""

	if elem is None or childTag is None:
		return None

	child = elem.xpath(f"netex:{childTag}", namespaces=NAMESPACES)
	return child[0].text if len(child) > 0 else None



def appendTag(elem: etree.Element, k: str, v: str):
	"""Appends a tag sub-element into given Element if tag is set

	Parameters
	----------
	elem: lxml.etree.Element
		The element to append tag into
	k: str
		The tag key
	v: str
		The tag value
	"""

	if v is not None and len(v.strip()) > 0:
		elem.append(E.tag(k=k, v=v.strip()))


def appendRefTags(netex: etree.Element, wdm: etree.Element):
	"""Appends reference and version tags to WDM Element based on Netex Element

	Parameters
	----------
	netex : lxml.etree.Element
		The XML element representing a single Netex Quay
	wdm : lxml.etree.Element
		The XML element embedding nodes + way representing a single quay in WDM format
	"""

	appendTag(wdm, "Ref:Import:Source", "NeTEx")
	appendTag(wdm, "Ref:Import:Id", netex.get("id"))
	if netex.get("version") != "any":
		appendTag(wdm, "Ref:Import:Version", netex.get("version"))


def appendAccessibilityTags(netex: etree.Element, wdm: etree.Element):
	"""Appends accessibility-related tags to WDM Element based on Netex AccessibilityAssessment

	Parameters
	----------
	netex : lxml.etree.Element
		The XML element representing a single Netex Quay
	wdm : lxml.etree.Element
		The XML element embedding nodes + way representing a single quay in WDM format
	"""

	valuesMapping = {"true": "Yes", "false": "No", "partial": "Limited"}

	appendTag(
		wdm, "WheelchairAccess:Description",
		getChildText(netex, "AccessibilityAssessment/netex:validityConditions/netex:ValidityCondition/netex:Description"))
	appendTag(
		wdm, "WheelchairAccess",
		valuesMapping.get(getChildText(netex, "AccessibilityAssessment/netex:limitations/netex:AccessibilityLimitation/netex:WheelchairAccess")))
	appendTag(
		wdm, "AudibleSignals",
		valuesMapping.get(getChildText(netex, "AccessibilityAssessment/netex:limitations/netex:AccessibilityLimitation/netex:AudibleSignalsAvailable")))
	appendTag(
		wdm, "VisualSigns",
		valuesMapping.get(getChildText(netex, "AccessibilityAssessment/netex:limitations/netex:AccessibilityLimitation/netex:VisualSignsAvailable")))


def appendQuay(wdm: etree.Element, netex: etree.Element, frameDefaults: etree.Element = None):
	"""Adds a Netex Quay into an existing WDM dataset

	Parameters
	----------
	wdm : lxml.etree.Element
		The WDM XML element in which new quay nodes and/or way will be inserted
	netex : lxml.etree.Element
		The XML element representing a single Netex Quay
	frameDefaults : lxml.etree.Element | None
		The default properties of the Netex frame
	"""

	quay = None


	# Read geometry
	centroid = netex.xpath("netex:Centroid", namespaces=NAMESPACES)
	polygon = netex.xpath("gml:Polygon", namespaces=NAMESPACES)

	if centroid:
		coords = netexCentroidToLonLat(centroid[0], getChildText(frameDefaults, "DefaultLocationSystem"))
		quay = E.node(
			id=str(int(getLastNewWDMElementId(wdm, "node") or "0") - 1),
			lon=coords[0],
			lat=coords[1]
		)

		if len(coords) == 3 and coords[2] is not None:
			quay.append(E.tag(k="Altitude", v=coords[2]))

		wdm.append(quay)

	elif polygon:
		quay = appendNetexPolygonToWdm(netex, wdm, getChildText(frameDefaults, "DefaultLocationSystem"))
		appendTag(quay, "InsideSpace", "Quay")

	else:
		raise Exception("No supported geometry found")


	# Read properties
	appendRefTags(netex, quay)
	appendAccessibilityTags(netex, quay)

	transport = getChildText(netex, "TransportMode")
	if transport is not None:
		transportMapping = {
			"bus": "Bus", "coach": "Coach", "ferry": "Water", "metro": "Metro",
			"rail": "Rail", "trolleyBus": "TrolleyBus", "tram": "Tram", "water": "Water",
			"cableway": "CableWay", "funicular": "Funicular"
		}
		appendTag(quay, "Quay", transportMapping.get(transport, "Other"))

	for k in ["Name", "Description", "PublicCode"]:
		appendTag(quay, k, getChildText(netex, k))

	outdoorMapping = { "outdoors": "Yes", "indoors": "No", "covered": "Covered" }
	appendTag(quay, "Outdoor", outdoorMapping.get(getChildText(netex, "Covered")))

	gated = getChildText(netex, "Gated")
	if gated == "gatedArea":
		appendTag(quay, "Gated", "Yes")

	lightingMapping = { "wellLit": "Yes", "unlit": "No", "poorlyLit": "Bad" }
	appendTag(quay, "Lighting", lightingMapping.get(getChildText(netex, "Lighting")))


	return wdm


def getStopPlaceQuays(netex: etree.Element, stopPlaceId: str):
	"""List IDs of quays linked to the given StopPlace

	Parameters
	----------
	netex: lxml.etree.Element
		The Netex XML root element
	stopPlaceId: str
		The StopPlace ID

	Returns
	-------
	list
		List of quays ID linked to the given StopPlace
	"""

	stopPlace = netex.xpath(f"{XPATH_FRAME_MEMBERS}/netex:StopPlace[@id=\"{stopPlaceId}\"]", namespaces=NAMESPACES)

	if len(stopPlace) != 1:
		raise Exception("StopPlace with ID "+stopPlaceId+" was not found")

	stopPlace = stopPlace[0]

	# Look for quays inside StopPlace
	quaysId = [ q.get("id") for q in stopPlace.xpath("netex:quays/netex:Quay", namespaces=NAMESPACES) ]
	quaysId += [ q.get("ref") for q in stopPlace.xpath("netex:quays/netex:QuayRef", namespaces=NAMESPACES) ]

	# Look for quays with ParentZoneRef
	quaysId += [ q.get("id") for q in netex.xpath(f"{XPATH_FRAME_MEMBERS}/netex:Quay[netex:ParentZoneRef[@ref=\"{stopPlaceId}\"]]", namespaces=NAMESPACES) ]
	quaysId += [ q.get("id") for q in netex.xpath(f"{XPATH_FRAME_MEMBERS}/netex:Quay[netex:SiteRef[@ref=\"{stopPlaceId}\"]]", namespaces=NAMESPACES) ]

	return sorted(list(set(quaysId)))

def getStopPlaceEntrances(netex: etree.Element, stopPlaceId: str):
	"""List IDs of entrances linked to the given StopPlace

	Parameters
	----------
	netex: lxml.etree.Element
		The Netex XML root element
	stopPlaceId: str
		The StopPlace ID

	Returns
	-------
	list
		List of entrances ID linked to the given StopPlace
	"""

	stopPlace = netex.xpath(f"{XPATH_FRAME_MEMBERS}/netex:StopPlace[@id=\"{stopPlaceId}\"]", namespaces=NAMESPACES)

	if len(stopPlace) != 1:
		raise Exception("StopPlace with ID "+stopPlaceId+" was not found")

	stopPlace = stopPlace[0]

	# Look for entrances inside StopPlace
	entrancesId = [ q.get("id") for q in stopPlace.xpath("netex:entrances/netex:Entrance", namespaces=NAMESPACES) ]
	entrancesId += [ q.get("id") for q in stopPlace.xpath("netex:entrances/netex:StopPlaceEntrance", namespaces=NAMESPACES) ]
	entrancesId += [ q.get("ref") for q in stopPlace.xpath("netex:entrances/netex:EntranceRef", namespaces=NAMESPACES) ]
	entrancesId += [ q.get("ref") for q in stopPlace.xpath("netex:entrances/netex:StopPlaceEntranceRef", namespaces=NAMESPACES) ]

	# Look for entrances with SiteRef
	entrancesId += [ q.get("id") for q in netex.xpath(f"{XPATH_FRAME_MEMBERS}/netex:Entrance[netex:SiteRef[@ref=\"{stopPlaceId}\"]]", namespaces=NAMESPACES) ]
	entrancesId += [ q.get("id") for q in netex.xpath(f"{XPATH_FRAME_MEMBERS}/netex:StopPlaceEntrance[netex:SiteRef[@ref=\"{stopPlaceId}\"]]", namespaces=NAMESPACES) ]

	return entrancesId

def toWDMCase(txt: str):
	"""Converts input text into full-title case"""
	if not isinstance(txt, str):
		return txt
	elif len(txt) < 2:
		return txt.title()
	else:
		return txt[0].upper() + txt[1:]


def appendStopPlace(wdm: etree.Element, netex: etree.Element, quays: list[str], entrances: list[str], frameDefaults: etree.Element = None):
	"""Adds a Netex StopPlace into an existing WDM dataset, if necessary.

	Parameters
	----------
	wdm : lxml.etree.Element
		The WDM XML element in which new StopPlace feature will be inserted
	netex : lxml.etree.Element
		The XML element representing a single Netex StopPlace
	quays : list[str]
		The list of Netex quays IDs linked to this StopPlace
	entrances : list[str]
		The list of Netex entrances IDs linked to this StopPlace		
	frameDefaults : lxml.etree.Element | None
		The default properties of the Netex frame
	"""

	stopPlaceType = getChildText(netex, "StopPlaceType")

	# No quays/entrances = no StopPlace
	if stopPlaceType is None or (len(quays) == 0 and len(entrances)==0):
		logging.info(f"Skipped empty stop place {netex.get('id')}")
		return


	# Convert as relation
	if stopPlaceType in ["onstreetBus", "onstreetTram", "ferryStop", "other"]:
		stop = E.relation(id=str(int(getLastNewWDMElementId(wdm, "relation") or "0") - 1))

		# Find members by mapping Netex ID into WDM ID
		for netexQuayId in quays:
			wdmQuay = wdm.xpath(f"""*[tag[@k="Ref:Import:Id" and @v="{netexQuayId}"]]""")
			if len(wdmQuay) != 1:
				raise Exception(f"Quay {netexQuayId} required in StopPlace {netex.get('id')} not found")

			wdmQuayId = wdmQuay[0].get("id")
			wdmQuayType = wdmQuay[0].tag
			stop.append(E.member(type=wdmQuayType, ref=wdmQuayId, role=""))

		wdm.append(stop)


	# Convert as POI
	else:
		# Read geometry
		centroid = netex.xpath("netex:Centroid", namespaces=NAMESPACES)
		polygon = netex.xpath("gml:Polygon", namespaces=NAMESPACES)

		if centroid:
			coords = netexCentroidToLonLat(centroid[0], getChildText(frameDefaults, "DefaultLocationSystem"))
			stop = E.node(
				id=str(int(getLastNewWDMElementId(wdm, "node") or "0") - 1),
				lon=coords[0],
				lat=coords[1]
			)

			if len(coords) == 3 and coords[2] is not None:
				stop.append(E.tag(k="Altitude", v=coords[2]))

			wdm.append(stop)

		elif polygon:
			stop = appendNetexPolygonToWdm(netex, wdm, getChildText(frameDefaults, "DefaultLocationSystem"))

		else:
			raise Exception("No supported geometry found")

		appendTag(stop, "PointOfInterest", "StopPlace")


	# Read properties
	appendRefTags(netex, stop)
	appendAccessibilityTags(netex, stop)
	appendTag(stop, "Name", getChildText(netex, "Name"))
	appendTag(stop, "Description", getChildText(netex, "Description"))
	appendTag(stop, "StopPlace", toWDMCase(stopPlaceType))

def appendEntrance(wdm: etree.Element, netex: etree.Element, frameDefaults: etree.Element = None):
	"""Adds a Netex Entrance into an existing WDM dataset

	Parameters
	----------
	wdm : lxml.etree.Element
		The WDM XML element in which new entrance nodes will be inserted
	netex : lxml.etree.Element
		The XML element representing a single Netex Entrance
	frameDefaults : lxml.etree.Element | None
		The default properties of the Netex frame
	"""

	entrance = None

	# Read geometry
	centroid = netex.xpath("netex:Centroid", namespaces=NAMESPACES)

	if not centroid:
		raise Exception("No supported geometry found")
	
	coords = netexCentroidToLonLat(centroid[0], getChildText(frameDefaults, "DefaultLocationSystem"))
	entrance = E.node(
		id=str(int(getLastNewWDMElementId(wdm, "node") or "0") - 1),
		lon=coords[0],
		lat=coords[1]
	)

	if len(coords) == 3 and coords[2] is not None:
		entrance.append(E.tag(k="Altitude", v=coords[2]))

	wdm.append(entrance)


	# Read properties
	appendRefTags(netex, entrance)
	appendAccessibilityTags(netex, entrance)

	name = getChildText(netex, "Label")
	if not name :
		name = getChildText(netex, "Name")
	appendTag(entrance, "Name", name)

	appendTag(entrance, "AddressLine", getChildText(netex, "PostalAddress/netex:AddressLine1"))

	for k in ["Description", "PublicCode", "Width", "Height"]:
		appendTag(entrance, k, getChildText(netex, k))

	booleanMapping = {"true": "Yes", "false": "No"}
	appendTag(entrance, "IsExit", booleanMapping.get(getChildText(netex, "IsExit")))
	appendTag(entrance, "IsEntry", booleanMapping.get(getChildText(netex, "IsEntry")))

	entrance_type = getChildText(netex, "EntranceType")
	if entrance_type == "opening":
		appendTag(entrance, "EntrancePassage", "Opening")
	if entrance_type == "gate":
		appendTag(entrance, "EntrancePassage", "Gate")	
	if entrance_type == "automaticDoor":
		appendTag(entrance, "EntrancePassage", "Door")
		appendTag(entrance, "AutomaticDoor", "Yes")
	if entrance_type == "revolvingDoor":
		appendTag(entrance, "EntrancePassage", "Door")
		appendTag(entrance, "Door", "Revolving")
	if entrance_type == "swingDoor":
		appendTag(entrance, "EntrancePassage", "Door")
		appendTag(entrance, "Door", "Swing")
	if entrance_type == "door":
		appendTag(entrance, "EntrancePassage", "Door")

	appendTag(entrance, "Entrance", "StopPlace")

	return wdm
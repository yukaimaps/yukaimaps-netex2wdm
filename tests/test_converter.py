#
# Tests for converter.py
#

import os
import pytest
from lxml import etree
from lxml.builder import ElementMaker, E
from lxml.doctestcompare import LXMLOutputChecker
from doctest import Example
from netex2wdm import converter

DATA_DIR = os.path.join(os.path.dirname(__file__), "./data")
EN = ElementMaker(namespace="http://www.netex.org.uk/netex")


def assertEqualsXml(validXmlName, toCheckXmlPath):
	"""Compares two given XML to check if they are identical

	Parameters
	----------
	validXmlName : str
		Name of the valid/reference XML stored in /tests/data folder
	toCheckXmlPath : str
		Full path to XML file to test
	"""
	validXmlPath = os.path.join(DATA_DIR, validXmlName)

	with open(validXmlPath, 'r') as validXmlFile:
		with open(toCheckXmlPath, 'r') as toCheckXmlFile:
			validXmlStr = validXmlFile.read()
			toCheckXmlStr = toCheckXmlFile.read()
			checker = LXMLOutputChecker()
			if not checker.check_output(validXmlStr, toCheckXmlStr, 0):
				message = checker.output_difference(Example("", validXmlStr), toCheckXmlStr, 0)
				raise AssertionError(message)


def test_readXMLFile():
	inputPath = os.path.join(DATA_DIR, "arret_fixture_0.xml")
	res = converter.readXMLFile(inputPath)
	assert res.getroot().tag == "{http://www.netex.org.uk/netex}PublicationDelivery"


def test_writeXMLFile(tmpdir):
	output = tmpdir.join("wdm.xml")
	xml = E.osm(E.node())
	validXmlStr = "<osm><node /></osm>"

	converter.writeXMLFile(xml, output)

	with open(output, 'r') as res:
		toCheckXmlStr = res.read()
		checker = LXMLOutputChecker()
		if not checker.check_output(validXmlStr, toCheckXmlStr, 0):
			message = checker.output_difference(Example("", validXmlStr), toCheckXmlStr, 0)
			raise AssertionError(message)


@pytest.mark.parametrize(("netexFile", "expectedWdmFile"), (
	("arret_fixture_0.xml", "wdm_arret_0.xml"),
	("arret_fixture_1.xml", "wdm_arret_1.xml"),
	("arret_fixture_2.xml", "wdm_arret_2.xml"),
	("mobiiti_extract.xml", "wdm_mobiiti_extract.xml"),
))
def test_transformNetexIntoWDM(tmpdir, netexFile, expectedWdmFile):
	resWdmFile = tmpdir.join("wdm.xml")
	netex = converter.readXMLFile(os.path.join(DATA_DIR, netexFile))
	resWdm = converter.transformNetexIntoWDM(netex)
	converter.writeXMLFile(resWdm, resWdmFile)
	assertEqualsXml(expectedWdmFile, resWdmFile)


@pytest.mark.parametrize(("location", "defaultSrs", "expected"), (
	(
		"""<pos xmlns="http://www.opengis.net/gml/3.2" srsName="EPSG:4326">-1.66668 48.12176</pos>""",
		None,
		["-1.6666800", "48.1217600", None]),
	(
		"""<pos xmlns="http://www.opengis.net/gml/3.2" srsName="EPSG:4326">-1.66668 48.12176 150.1</pos>""",
		None,
		["-1.6666800", "48.1217600", "150.1"]),
	(
		"""<pos xmlns="http://www.opengis.net/gml/3.2" srsName="EPSG:2154">898470.0224093036 6251796.754053905</pos>""",
		None,
		["5.4464584", "43.3376458", None]),
	(
		"""<pos xmlns="http://www.opengis.net/gml/3.2">898470.0224093036 6251796.754053905</pos>""",
		"EPSG:2154",
		["5.4464584", "43.3376458", None]),
))
def test_gmlPosToLonLat(location, defaultSrs, expected):
	res = converter.gmlPosToLonLat(etree.XML(location), defaultSrs)
	assert res == expected


@pytest.mark.parametrize(("location", "defaultSrs", "expected"), (
	(
		"""<gml:pos srsName="EPSG:4326">-1.66668 48.12176 150.1</gml:pos>""",
		None,
		["-1.6666800", "48.1217600", "150.1"]),
	(
		"""<Longitude>-1.66668</Longitude><Latitude>48.12176</Latitude>""",
		None,
		["-1.6666800", "48.1217600", None]),
	(
		"""<Longitude>898470.0224093036</Longitude><Latitude>6251796.754053905</Latitude>""",
		"EPSG:2154",
		["5.4464584", "43.3376458", None]),
	(
		"""<Longitude>-1.66668</Longitude><Latitude>48.12176</Latitude><Altitude>150.1</Altitude>""",
		None,
		["-1.6666800", "48.1217600", "150.1"]),
))
def test_netexCentroidToLonLat(location, defaultSrs, expected):
	centroid = etree.XML(f"""
		<Centroid xmlns="http://www.netex.org.uk/netex" xmlns:gml="http://www.opengis.net/gml/3.2">
			<Location>{location}</Location>
		</Centroid>""")

	res = converter.netexCentroidToLonLat(centroid, srs = defaultSrs)
	assert res == expected


@pytest.mark.parametrize(("elemType", "existingIds", "expected"), (
	("node", [], None),
	("node", ["1", "2", "3"], None),
	("node", ["-1", "1"], "-1"),
	("node", ["-1", "12", "-50"], "-50"),
	("way", [], None),
	("way", ["-1", "12", "-50"], "-50"),
))
def test_getLastNewWDMElementId(elemType, existingIds, expected):
	wdm = E.osm()

	# Create existing data
	for v in existingIds:
		etree.SubElement(wdm, elemType, id=v)

	res = converter.getLastNewWDMElementId(wdm, elemType)
	assert res == expected



@pytest.mark.parametrize(("polygon", "defaultSrs", "expectedNodes", "lastNodeId", "lastWayId"), (
	("""<Polygon xmlns="http://www.opengis.net/gml/3.2"><exterior><LinearRing>
			<pos>-120.000000 65.588264</pos>
			<pos>-120.003571 65.590782</pos>
			<pos>-120.011292 65.590965</pos>
			<pos>-120.000000 65.588264</pos>
		</LinearRing></exterior></Polygon>""",
		None,
		[["-120.0000000", "65.5882640"], ["-120.0035710", "65.5907820"], ["-120.0112920", "65.5909650"], ["-120.0000000", "65.5882640"]],
		0,
		0),
	("""<Polygon xmlns="http://www.opengis.net/gml/3.2"><exterior><LinearRing>
			<pos>-3187012.484630801 12604711.652090173</pos>
			<pos>-3186715.2114284215 12604891.34779405</pos>
			<pos>-3186698.380238443 12605271.675498795</pos>
			<pos>-3187012.484630801 12604711.652090173</pos>
		</LinearRing></exterior></Polygon>""",
		"EPSG:2154",
		[["-120.0000000", "65.5882640"], ["-120.0035710", "65.5907820"], ["-120.0112920", "65.5909650"], ["-120.0000000", "65.5882640"]],
		0,
		0),
	("""<Polygon xmlns="http://www.opengis.net/gml/3.2" srsName="EPSG:2154"><exterior><LinearRing>
			<pos>-3187012.484630801 12604711.652090173</pos>
			<pos>-3186715.2114284215 12604891.34779405</pos>
			<pos>-3186698.380238443 12605271.675498795</pos>
			<pos>-3187012.484630801 12604711.652090173</pos>
		</LinearRing></exterior></Polygon>""",
		None,
		[["-120.0000000", "65.5882640"], ["-120.0035710", "65.5907820"], ["-120.0112920", "65.5909650"], ["-120.0000000", "65.5882640"]],
		0,
		0),
	("""<Polygon xmlns="http://www.opengis.net/gml/3.2"><exterior><LinearRing>
			<pos>-120.000000 65.588264</pos>
			<pos>-120.003571 65.590782</pos>
			<pos>-120.011292 65.590965</pos>
			<pos>-120.000000 65.588264</pos>
		</LinearRing></exterior></Polygon>""",
		None,
		[["-120.0000000", "65.5882640"], ["-120.0035710", "65.5907820"], ["-120.0112920", "65.5909650"], ["-120.0000000", "65.5882640"]],
		-10,
		-20),
))
def test_appendNetexPolygonToWdm(polygon, defaultSrs, expectedNodes, lastNodeId, lastWayId):
	netex = EN.Quay(etree.XML(polygon))
	wdm = E.osm()

	if lastNodeId < 0:
		wdm.append(E.node(id=str(lastNodeId)))

	if lastWayId < 0:
		wdm.append(E.way(id=str(lastWayId)))

	res = converter.appendNetexPolygonToWdm(netex, wdm, defaultSrs)

	if lastNodeId < 0:
		assert len(wdm.xpath("node")) == len(expectedNodes)
	else:
		assert len(wdm.xpath("node")) == len(expectedNodes) - 1

	if lastWayId < 0:
		assert len(wdm.xpath("way")) == 2
	else:
		assert len(wdm.xpath("way")) == 1

	way = wdm.xpath(f"way[@id={lastWayId-1}]")[0]
	assert way.get("id") == str(lastWayId - 1)
	assert len(way.xpath("nd")) == len(expectedNodes)

	resNd = way.xpath("nd")

	assert resNd[0].get("ref") == resNd[-1].get("ref")

	for i in range(0, len(expectedNodes)):
		expectedNd = expectedNodes[i]
		resNdRef = resNd[i].get("ref")

		if i == len(expectedNodes) - 1:
			assert resNdRef == str(lastNodeId - 1)
		else:
			assert resNdRef == str(lastNodeId - 1 - i)

		nd = wdm.xpath(f"node[@id={resNdRef}]")
		assert len(nd) == 1
		nd = nd[0]
		assert nd.get("lon") == expectedNd[0]
		assert nd.get("lat") == expectedNd[1]


def test_getChildText_exists():
	elem = EN.Quay(EN.Name("bla"))
	res = converter.getChildText(elem, "Name")
	assert res == "bla"


def test_getChildText_unset():
	elem = EN.Quay()
	res = converter.getChildText(elem, "Name")
	assert res is None


def test_getChildText_emptyElem():
	res = converter.getChildText(None, "Name")
	assert res is None


def test_appendTag_set():
	elem = E.node()
	converter.appendTag(elem, "Name", "Bla")
	assert elem[0].tag == "tag"
	assert elem[0].get("k") == "Name"
	assert elem[0].get("v") == "Bla"


def test_appendTag_unset():
	elem = E.node()
	converter.appendTag(elem, "Name", None)
	assert len(elem) == 0


def test_appendTag_empty():
	elem = E.node()
	converter.appendTag(elem, "Name", "")
	assert len(elem) == 0


def test_appendRefTags_basic():
	netex = EN.Quay(id="FR:Quay:3643:", version="1")
	elem = E.node()
	converter.appendRefTags(netex, elem)

	assertTag(elem, "Ref:Import:Source", "NeTEx")
	assertTag(elem, "Ref:Import:Id", "FR:Quay:3643:")
	assertTag(elem, "Ref:Import:Version", "1")


def test_appendRefTags_any():
	netex = EN.Quay(id="FR:Quay:3643:", version="any")
	elem = E.node()
	converter.appendRefTags(netex, elem)

	assertTag(elem, "Ref:Import:Source", "NeTEx")
	assertTag(elem, "Ref:Import:Id", "FR:Quay:3643:")
	assertTag(elem, "Ref:Import:Version", None)


@pytest.mark.parametrize(("vNetex", "vWdm"), (
	("true", "Yes"),
	("false", "No"),
	("partial", "Limited"),
	("unknown", None),
))
def test_appendAccessibilityTags(vNetex, vWdm):
	netex = EN.Quay(EN.AccessibilityAssessment(
		EN.validityConditions(EN.ValidityCondition(EN.Description("Sympa"))),
		EN.limitations(EN.AccessibilityLimitation(
			EN.WheelchairAccess(vNetex),
			EN.AudibleSignalsAvailable(vNetex),
			EN.VisualSignsAvailable(vNetex)
		))
	))

	elem = E.node()
	converter.appendAccessibilityTags(netex, elem)

	assertTag(elem, "WheelchairAccess:Description", "Sympa")
	assertTag(elem, "WheelchairAccess", vWdm)
	assertTag(elem, "AudibleSignals", vWdm)
	assertTag(elem, "VisualSigns", vWdm)


def assertTag(elem, k, v):
	tag = elem.xpath(f"tag[@k='{k}']")
	if v is not None:
		assert len(tag) == 1
		assert tag[0].get("v") == v
	else:
		assert len(tag) == 0


def test_appendQuay_node():
	netex = """
		<Quay xmlns="http://www.netex.org.uk/netex" xmlns:gml="http://www.opengis.net/gml/3.2" id="FR:Quay:3643:" version="any">
			<Name>De Roux Garlaban</Name>
			<TransportMode>bus</TransportMode>
			<Description>Un arrêt tip-top</Description>
			<PublicCode>3643</PublicCode>
			<Covered>indoors</Covered>
			<Gated>gatedArea</Gated>
			<Lighting>wellLit</Lighting>
			<Centroid>
				<Location>
					<gml:pos>898470.0224093036 6251796.754053905 248.1</gml:pos>
				</Location>
			</Centroid>
			<AccessibilityAssessment id="FR:AccessibilityAssessment:1441_0:" version="any">
				<MobilityImpairedAccess>partial</MobilityImpairedAccess>
				<limitations>
					<AccessibilityLimitation>
						<WheelchairAccess>true</WheelchairAccess>
						<AudibleSignalsAvailable>unknown</AudibleSignalsAvailable>
						<VisualSignsAvailable>false</VisualSignsAvailable>
					</AccessibilityLimitation>
				</limitations>
			</AccessibilityAssessment>
		</Quay>"""
	netexElement = etree.XML(netex)

	frameDefaults = EN.FrameDefaults(EN.DefaultLocationSystem("EPSG:2154"))

	wdm = E.osm(E.node(id="-1"), version="0.6")
	converter.appendQuay(wdm, netexElement, frameDefaults)
	quays = wdm.xpath("node[@id=-2]")

	assert len(quays) == 1

	quay = quays[0]
	assert quay.get("id") == "-2"
	assert quay.get("lon") == "5.4464584"
	assert quay.get("lat") == "43.3376458"

	assertTag(quay, "Ref:Import:Source", "NeTEx")
	assertTag(quay, "Ref:Import:Id", "FR:Quay:3643:")
	assertTag(quay, "Ref:Import:Version", None)
	assertTag(quay, "Quay", "Bus")
	assertTag(quay, "Altitude", "248.1")
	assertTag(quay, "Name", "De Roux Garlaban")
	assertTag(quay, "Description", "Un arrêt tip-top")
	assertTag(quay, "PublicCode", "3643")
	assertTag(quay, "Outdoor", "No")
	assertTag(quay, "Gated", "Yes")
	assertTag(quay, "Lighting", "Yes")
	assertTag(quay, "WheelchairAccess", "Yes")
	assertTag(quay, "WheelchairAccess:Description", None)
	assertTag(quay, "AudibleSignals", None)
	assertTag(quay, "VisualSigns", "No")
	assertTag(quay, "InsideSpace", None)


def test_appendQuay_transportmode():
	netex = """
		<Quay xmlns="http://www.netex.org.uk/netex" xmlns:gml="http://www.opengis.net/gml/3.2" id="FR:Quay:3643:" version="any">
			<Name>De Roux Garlaban</Name>
			<TransportMode>something else</TransportMode>
			<Centroid>
				<Location>
					<gml:pos>898470.0224093036 6251796.754053905 248.1</gml:pos>
				</Location>
			</Centroid>
		</Quay>"""
	netexElement = etree.XML(netex)

	frameDefaults = EN.FrameDefaults(EN.DefaultLocationSystem("EPSG:2154"))

	wdm = E.osm(E.node(id="-1"), version="0.6")
	converter.appendQuay(wdm, netexElement, frameDefaults)
	quays = wdm.xpath("node[@id=-2]")

	assert len(quays) == 1
	quay = quays[0]
	assertTag(quay, "Quay", "Other")


def test_appendQuay_poly():
	netex = """
		<Quay xmlns="http://www.netex.org.uk/netex" xmlns:gml="http://www.opengis.net/gml/3.2" id="FR:Quay:3643:" version="any">
			<TransportMode>bus</TransportMode>
			<gml:Polygon><gml:exterior><gml:LinearRing>
				<gml:pos>-120.000000 65.588264</gml:pos>
				<gml:pos>-120.003571 65.590782</gml:pos>
				<gml:pos>-120.011292 65.590965</gml:pos>
				<gml:pos>-120.000000 65.588264</gml:pos>
			</gml:LinearRing></gml:exterior></gml:Polygon>
		</Quay>"""
	netexElement = etree.XML(netex)

	frameDefaults = EN.FrameDefaults(EN.DefaultLocationSystem("EPSG:4326"))

	wdm = E.osm(version="0.6")
	converter.appendQuay(wdm, netexElement, frameDefaults)
	quays = wdm.xpath("way")

	assert len(quays) == 1

	quay = quays[0]

	assertTag(quay, "Ref:Import:Source", "NeTEx")
	assertTag(quay, "Ref:Import:Id", "FR:Quay:3643:")
	assertTag(quay, "Ref:Import:Version", None)
	assertTag(quay, "Quay", "Bus")
	assertTag(quay, "InsideSpace", "Quay")

	# Check associated nodes
	nds = quay.xpath("nd")
	assert len(nds) == 4

	nodes = [ wdm.xpath(f"node[@id={nd.get('ref')}]")[0] for nd in nds ]

	assert nodes[0].get("lon") == "-120.0000000"
	assert nodes[0].get("lat") == "65.5882640"
	assert nodes[1].get("lon") == "-120.0035710"
	assert nodes[1].get("lat") == "65.5907820"
	assert nodes[2].get("lon") == "-120.0112920"
	assert nodes[2].get("lat") == "65.5909650"
	assert nodes[3].get("lon") == "-120.0000000"
	assert nodes[3].get("lat") == "65.5882640"


def test_getStopPlaceQuays():
	netex = """<PublicationDelivery xmlns="http://www.netex.org.uk/netex"><dataObjects><GeneralFrame><members>
		<Quay id="q2"></Quay>

		<Quay id="q3">
			<ParentZoneRef ref="sp1" />
		</Quay>

		<Quay id="q4">
			<SiteRef ref="sp1" />
		</Quay>

		<Quay id="q5"></Quay>

		<StopPlace id="sp1">
			<quays>
				<Quay id="q1"></Quay>
				<QuayRef ref="q2"></QuayRef>
			</quays>
		</StopPlace>

		<StopPlace id="sp2"></StopPlace>
	</members></GeneralFrame></dataObjects></PublicationDelivery>"""

	res1 = converter.getStopPlaceQuays(etree.XML(netex), "sp1")
	assert sorted(res1) == ["q1", "q2", "q3", "q4"]

	res2 = converter.getStopPlaceQuays(etree.XML(netex), "sp2")
	assert res2 == []

def test_getStopPlaceEntrances():
	netex = """<PublicationDelivery xmlns="http://www.netex.org.uk/netex"><dataObjects><GeneralFrame><members>
		<Entrance id="e2"></Entrance>

		<Entrance id="e4">
			<SiteRef ref="sp1" />
		</Entrance>

		<StopPlaceEntrance id="e3">
			<SiteRef ref="sp1" />
		</StopPlaceEntrance>

		<Entrance id="e5"></Entrance>

		<StopPlaceEntrance id="e6"></StopPlaceEntrance>

		<StopPlace id="sp1">
			<entrances>
				<Entrance id="e1"></Entrance>
				<EntranceRef ref="e2"></EntranceRef>
				<StopPlaceEntranceRef ref="e6"></StopPlaceEntranceRef>
			</entrances>
		</StopPlace>

		<StopPlace id="sp2"></StopPlace>
	</members></GeneralFrame></dataObjects></PublicationDelivery>"""

	res1 = converter.getStopPlaceEntrances(etree.XML(netex), "sp1")
	assert sorted(res1) == ["e1", "e2", "e3", "e4", "e6"]

	res2 = converter.getStopPlaceEntrances(etree.XML(netex), "sp2")
	assert res2 == []

@pytest.mark.parametrize(("txt", "res"), (
	("test", "Test"),
	("testTwo", "TestTwo"),
	("", ""),
	(None, None),
))
def test_toWDMCase(txt, res):
	assert converter.toWDMCase(txt) == res


def test_appendStopPlace_void():
	netex = """<StopPlace id="sp1" xmlns="http://www.netex.org.uk/netex">
	</StopPlace>"""
	netexElement = etree.XML(netex)

	wdm = E.osm(version="0.6")
	converter.appendStopPlace(wdm, netexElement, [], [])

	assert len(wdm) == 0


def test_appendStopPlace_poi():
	netex = """<StopPlace id="sp1" xmlns="http://www.netex.org.uk/netex" xmlns:gml="http://www.opengis.net/gml/3.2">
		<Name>Caillol</Name>
		<Centroid><Location><gml:pos>898416.1318965019 6247027.335379389</gml:pos></Location></Centroid>
		<TransportMode>tram</TransportMode>
		<StopPlaceType>tramStation</StopPlaceType>
		<AccessibilityAssessment id="FR:AccessibilityAssessment:2053_0:" version="any">
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>true</WheelchairAccess>
				</AccessibilityLimitation>
			</limitations>
		</AccessibilityAssessment>
	</StopPlace>"""
	netexElement = etree.XML(netex)

	frameDefaults = EN.FrameDefaults(EN.DefaultLocationSystem("EPSG:2154"))

	wdm = E.osm(version="0.6")
	converter.appendStopPlace(wdm, netexElement, ["q1"], [], frameDefaults)

	stopPlace = wdm.xpath("node[@id=-1]")

	assert len(stopPlace) == 1
	stopPlace = stopPlace[0]

	assert stopPlace.get("lon") == "5.4439755"
	assert stopPlace.get("lat") == "43.2947767"

	assertTag(stopPlace, "Ref:Import:Source", "NeTEx")
	assertTag(stopPlace, "Ref:Import:Id", "sp1")
	assertTag(stopPlace, "Ref:Import:Version", None)
	assertTag(stopPlace, "PointOfInterest", "StopPlace")
	assertTag(stopPlace, "StopPlace", "TramStation")
	assertTag(stopPlace, "WheelchairAccess", "Yes")
	assertTag(stopPlace, "Name", "Caillol")


def test_appendStopPlace_rel():
	netex = """<StopPlace id="sp1" xmlns="http://www.netex.org.uk/netex" xmlns:gml="http://www.opengis.net/gml/3.2">
		<Name>Caillol</Name>
		<TransportMode>bus</TransportMode>
		<StopPlaceType>onstreetBus</StopPlaceType>
		<AccessibilityAssessment id="FR:AccessibilityAssessment:2053_0:" version="any">
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>true</WheelchairAccess>
				</AccessibilityLimitation>
			</limitations>
		</AccessibilityAssessment>
		<quays>
			<QuayRef ref="FR:Quay:3643:" />
		</quays>
	</StopPlace>"""
	netexElement = etree.XML(netex)

	wdmQuay = """<node id="-42" lat="43.3376458" lon="5.4464584">
		<tag k="Ref:Import:Source" v="NeTEx" />
		<tag k="Ref:Import:Id" v="FR:Quay:3643:" />
		<tag k="Quay" v="Bus" />
	</node>"""

	wdm = E.osm(version="0.6")
	wdm.append(etree.XML(wdmQuay))
	converter.appendStopPlace(wdm, netexElement, ["FR:Quay:3643:"], ["FR:StopPlaceEntrance:23:"])

	print(etree.tostring(wdm))
	stopPlace = wdm.xpath("relation[@id=-1]")

	assert len(stopPlace) == 1
	stopPlace = stopPlace[0]

	assertTag(stopPlace, "Ref:Import:Source", "NeTEx")
	assertTag(stopPlace, "Ref:Import:Id", "sp1")
	assertTag(stopPlace, "Ref:Import:Version", None)
	assertTag(stopPlace, "StopPlace", "OnstreetBus")
	assertTag(stopPlace, "WheelchairAccess", "Yes")
	assertTag(stopPlace, "Name", "Caillol")

	members = stopPlace.xpath("member")

	assert len(members) == 1
	member = members[0]
	assert member.get("type") == "node"
	assert member.get("ref") == "-42"
	assert member.get("role") == ""

def test_appendEntrance_SPEntrance():
	netex = """
		<StopPlaceEntrance xmlns="http://www.netex.org.uk/netex" xmlns:gml="http://www.opengis.net/gml/3.2" version="2" id="JungleBus:Entrance:n5889:LOC">
			<Centroid>
				<Location id="loc_entrance_id">
					<Longitude>2.068554</Longitude>
					<Latitude>48.764788</Latitude>
				</Location>
			</Centroid>
			<PostalAddress version="any" id="JungleBus:PostalAddress:2">
				<AddressLine1>23 Rue de la route</AddressLine1>
				<PostCode>52364</PostCode>
			</PostalAddress>
			<AccessibilityAssessment version="any" id="JungleBus:AccessibilityAssessment:80:">
				<MobilityImpairedAccess>true</MobilityImpairedAccess>
				<limitations>
				<AccessibilityLimitation id="JungleBus:AccessibilityLimitation:80:">
					<WheelchairAccess>unknown</WheelchairAccess>
					<StepFreeAccess>true</StepFreeAccess>
				</AccessibilityLimitation>
				</limitations>
			</AccessibilityAssessment>
			<PublicCode>2</PublicCode>
			<Label>place de la gare</Label>
			<EntranceType>automaticDoor</EntranceType>
			<IsEntry>true</IsEntry>
			<IsExit>true</IsExit>
			<Width>12</Width>
			<Height>150</Height>
		</StopPlaceEntrance>"""
	netexElement = etree.XML(netex)

	frameDefaults = EN.FrameDefaults(EN.DefaultLocationSystem("EPSG:4326"))

	wdm = E.osm(version="0.6")
	converter.appendEntrance(wdm, netexElement, frameDefaults)

	entrances = wdm.xpath("node[@id=-1]")
	print(etree.tostring(wdm))

	assert len(entrances) == 1

	entrance = entrances[0]
	assert entrance.get("id") == "-1"
	assert entrance.get("lon") == "2.0685540"
	assert entrance.get("lat") == "48.7647880"

	assertTag(entrance, "Ref:Import:Source", "NeTEx")
	assertTag(entrance, "Ref:Import:Id", "JungleBus:Entrance:n5889:LOC")
	assertTag(entrance, "Ref:Import:Version", "2")
	assertTag(entrance, "PublicCode", "2")
	assertTag(entrance, "WheelchairAccess", None)
	assertTag(entrance, "Name", "place de la gare")
	assertTag(entrance, "AddressLine", "23 Rue de la route")
	assertTag(entrance, "Width", "12")
	assertTag(entrance, "Height", "150")
	assertTag(entrance, "IsEntry", "Yes")
	assertTag(entrance, "IsExit", "Yes")
	assertTag(entrance, "EntrancePassage", "Door")
	assertTag(entrance, "AutomaticDoor", "Yes")
	assertTag(entrance, "Entrance", "StopPlace")

def test_appendEntrance_simple():
	netex = """
		<Entrance xmlns="http://www.netex.org.uk/netex" xmlns:gml="http://www.opengis.net/gml/3.2" id="JungleBus:Entrance:n5824189:LOC">
			<Centroid>
				<Location id="loc_entrance_id">
				<Longitude>2.068554</Longitude>
				<Latitude>48.764788</Latitude>
				</Location>
			</Centroid>
			<Name>Sortie</Name>
			<IsEntry>false</IsEntry>
			<IsExit>true</IsExit>
		</Entrance>"""
	netexElement = etree.XML(netex)

	frameDefaults = None

	wdm = E.osm(version="0.6")
	converter.appendEntrance(wdm, netexElement, frameDefaults)

	entrances = wdm.xpath("node[@id=-1]")
	print(etree.tostring(wdm))

	assert len(entrances) == 1

	entrance = entrances[0]
	assert entrance.get("id") == "-1"
	assert entrance.get("lon") == "2.0685540"
	assert entrance.get("lat") == "48.7647880"

	assertTag(entrance, "Ref:Import:Source", "NeTEx")
	assertTag(entrance, "Ref:Import:Id", "JungleBus:Entrance:n5824189:LOC")
	assertTag(entrance, "Ref:Import:Version", None)
	assertTag(entrance, "PublicCode", None)
	assertTag(entrance, "EntrancePassage", None)
	assertTag(entrance, "Name", "Sortie")
	assertTag(entrance, "IsEntry", "No")
	assertTag(entrance, "IsExit", "Yes")
	assertTag(entrance, "Entrance", "StopPlace")
